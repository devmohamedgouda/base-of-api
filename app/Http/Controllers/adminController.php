<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\adminRepositoryInterface;


class adminController extends Controller
{
    /** Group of model as vars */
    protected $adminRepositoryInterface;

    /** Construct to handel inject models */
    public function __construct(adminRepositoryInterface $adminRepositoryInterface){
        $this->adminRepositoryInterface = $adminRepositoryInterface;
    }

    public function getUsers(){
        return $this->adminRepositoryInterface->getUsers();
    }
}
