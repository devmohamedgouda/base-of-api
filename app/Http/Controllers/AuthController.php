<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Http\Controllers\api\Traits\ApiResponseTrait;
use App\Http\Controllers\api\Traits\tokenTrait;


use App\User;

class AuthController extends Controller
{
    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    use tokenTrait;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

   
    public function login(Request $request){
        /**
         * Build:[
         *  request validation
         *  Login data
         * ]
        */
        $Validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if($Validator->fails()){
            return $this->apiResponse(422 , "Validation Errors", $Validator->errors());
        }

        $credentials = $request->only('email', 'password');

        if ($token = auth()->attempt($credentials)){
            return $this->respondWithToken($token);
        }
        return $this->apiResponse(400 , 'Email or password are not correct');

           
            
       
    }



     // User Data Function
     public function user_data($id , $token){
         /** Build:
          * select User Data.
          * return user data
          */
        $user = User::where("id" , $id)->first();

        $data = [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'token' => $token,

        ];
        return $data;
    }


    // Respond With Token
    protected function respondWithToken($token){
        /** Build:
         * call user_data function.
         * return data.
         */
        $data =  $this->user_data(auth()->user()->id , $token);

        return $this->apiResponse(200 , 'Successfully', null ,$data);

    }


    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }
}
