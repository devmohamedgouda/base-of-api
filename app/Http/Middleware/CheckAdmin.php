<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Traits\tokenTrait;
use App\Http\Traits\ApiResponseTrait;

class CheckAdmin
{
    use tokenTrait;
    use ApiResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_data = $this->getAuthenticatedUser();
        if($user_data->role == 2){
            return $next($request);
        }else{
            return $this->apiResponse(400 , "You do not have this authority because you are not Admin" );
        }
    }
}
