<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\adminRepositoryInterface;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Traits\adminTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\User;

class adminRepository implements adminRepositoryInterface{

    // Use Trair To Desgin API's.
    use ApiResponseTrait;
    use adminTrait;
    
    /** Group of model as vars */
    protected $users_model;
    
    /** Construct to handel inject models */
    public function __construct(User $User){
        $this->users_model = $User;
    }

    public function getUsers(){

        $users = $this->users_model::get();

        if($users){
            return $this->apiResponse(200, "success", null, $users);
        }else{
            return $this->apiResponse(422, "unkown errors",);
        }
    } 

}