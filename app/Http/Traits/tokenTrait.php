<?php
namespace App\Http\Traits;

use JWTAuth;

trait tokenTrait{


    public function getAuthenticatedUser(){

        /** Build:
         * check if user token found or not.
         * check if token expired or not.
         * check if token invalid or not.
         * check if token absent or not.
         */

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return $this->apiResponse(422 , "user_not_found" ,$e->getStatusCode());
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return $this->apiResponse(422 , "token_expired" ,$e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return $this->apiResponse(422 , "token_invalid" ,$e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
        }

        return $user;
    }  

}

?>